
## Build and Runtime Requirements
 Xcode 12.0 or later
 iOS 14.5 or later
 OS X 11.4 or later


## Configuring the Project

This project has used 3rd party library via podFile

Library included:

NotificationBannerSwift - This library is used for showing error on UI if any

Kingfisher -  This library is used for showing images on UI with cache and other features.

To install pod on machine follow below steps:
1. Open Terminal
2.Go to project directory
3. Type 'pod install' in terminal
4.Once all installation is done
5.open .xcworkspace file

## Once open project select Debug configuration while running project on iPhone Device.


## Swift Features used in APP

The Lister sample leverages many features unique to Swift, including the following:

#### Clousers

Escaping closures are used to get data in View Model class from APIManger class.

#### Enum Constants

To get client id through out the app we have used static let variables enum

#### Extensions 

To add additional functionality to a class we have used Extensions

#### Protocol

Protocols are used for transferring data between classes.



