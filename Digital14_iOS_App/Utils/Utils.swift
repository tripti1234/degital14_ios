//
//  Utils.swift
//  Digital14_iOS_App
//
//  Created by Tripti Kumari on 04/08/21.
//

import Foundation
import UIKit
import NotificationBannerSwift

class Utils {
     
    static let shared = Utils()
    var banner = NotificationBanner(title: "")
    
    func showLoader() {
        DispatchQueue.main.async {
            visibleViewController()?.view.aj_showDotLoadingIndicator()
        }
    }
    
    func hideLoader() {
        DispatchQueue.main.async {
            visibleViewController()?.view.aj_hideDotLoadingIndicator()
        }
    }
}

extension UIViewController {
    
    func alertNotification(msg: String, with duration: Double = 2) {
        DispatchQueue.main.async {
            Utils.shared.banner.duration = duration
            Utils.shared.banner.titleLabel?.text = msg
            Utils.shared.banner.backgroundColor = .red
            Utils.shared.banner.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 15)
            Utils.shared.banner.titleLabel?.textColor = .white
            Utils.shared.banner.show(queuePosition: .back, bannerPosition: .bottom)
        }
    }
}

extension String {
    
    func toDate(format: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let dt = formatter.date(from: self)
        return dt ?? Date()
    }
}

struct EventsDate {
    static let baseDate = "yyyy-MM-dd'T'HH:mm:ss"
    static let mmmDDyyyyHHmm = "MMM dd, yyyy 'at' hh:mm a"
    private init() {}
}

extension Date {
    
    func toString(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale.current
        return formatter.string(from: self)
    }
}
