//
//  EventViewModel.swift
//  Digital14_iOS_App
//
//  Created by Tripti Kumari on 04/08/21.
//

import Foundation

protocol EventViewModelDelegate {
    func getEvent(events:Events)
}

class EventViewModel {
    var delegate : EventViewModelDelegate?
    
    init(delegate:EventViewModelDelegate) {
        self.delegate = delegate
    }
    
    var events:Events? {
        didSet{
            if let events = events {
                self.delegate?.getEvent(events: events)
            }
        }
    }
    
    func getEvent(searchString:String?){
        AppDelegate.shared.apiManger.getEvents(searchString: searchString) { events in
            self.events = events
        }
    }
}
