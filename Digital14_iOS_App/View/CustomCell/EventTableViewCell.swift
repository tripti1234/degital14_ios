//
//  EventTableViewCell.swift
//  Digital14_iOS_App
//
//  Created by Tripti Kumari on 04/08/21.
//

import UIKit
import Kingfisher

class EventTableViewCell: UITableViewCell {
    
    @IBOutlet weak var eventImageView:UIImageView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var locationLabel:UILabel!
    @IBOutlet weak var dateAndTimeLabel:UILabel!
    
    static let identifier = "EventTableViewCell"
    
    var event:ResultModel? {
        didSet {
            self.titleLabel.text = event?.title
            self.locationLabel.text = event?.venue?.display_location
            let date = event?.datetime_local?.toDate(format: EventsDate.baseDate)
            self.dateAndTimeLabel.text = date?.toString(format: EventsDate.mmmDDyyyyHHmm)
            self.eventImageView.kf.setImage(with: URL(string: event?.performers?.first?.image ?? ""))
        }
    }

    override func awakeFromNib() {
        self.contentView.backgroundColor = .white
        self.titleLabel.textColor = .black
        self.locationLabel.textColor = .gray
        self.dateAndTimeLabel.textColor = .gray
        self.eventImageView.layer.cornerRadius = 5
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
