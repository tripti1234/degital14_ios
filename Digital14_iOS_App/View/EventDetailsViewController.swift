//
//  EventDetailsViewController.swift
//  Digital14_iOS_App
//
//  Created by Tripti Kumari on 04/08/21.
//

import UIKit
import Kingfisher

class EventDetailsViewController: UIViewController {
    @IBOutlet weak var eventImageView:UIImageView!
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var locationLabel:UILabel!

    static let sequeIdentifier = "Details"
    var event:ResultModel? {
        didSet{
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUPScreen()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
            self.view.backgroundColor = .white
            self.eventImageView.layer.cornerRadius = 5
            self.eventImageView.clipsToBounds = true
        }
    
    
    func setUPScreen(){
        
        navigationController?.navigationBar.barTintColor = .white
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
            label.backgroundColor = UIColor.clear
            label.numberOfLines = 0
            label.textAlignment = NSTextAlignment.center
            label.text = event?.title
            label.textColor = .black
            label.font = UIFont(name: "HelveticaNeue-Bold", size: 17) ?? UIFont.systemFont(ofSize: 17.0)
            self.navigationItem.titleView = label
        self.eventImageView.kf.setImage(with: URL(string: event?.performers?.first?.image ?? ""))
        let date = event?.datetime_local?.toDate(format: EventsDate.baseDate)
        self.dateLabel.text = date?.toString(format: EventsDate.mmmDDyyyyHHmm)
        self.locationLabel.text = event?.venue?.display_location
    }
    

}
