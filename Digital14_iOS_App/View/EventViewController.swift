//
//  ViewController.swift
//  Digital14_iOS_App
//
//  Created by Tripti Kumari on 04/08/21.
//

import UIKit

class EventViewController: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var searchTextField : UISearchTextField!
    private var viewModel:EventViewModel?
    
    
    var eventsDict:Events? {
        didSet{
            if let _ = eventsDict {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpScreen()
        viewModel = EventViewModel(delegate: self)
        self.getEvents(searchString: "")
    }

    func getEvents(searchString:String?){
        viewModel?.getEvent(searchString: searchString)
    }
    
    func setUpScreen(){
        
        if let textfield = searchTextField.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = .gray
            textfield.attributedPlaceholder = NSAttributedString(string:"Search Events...", attributes:[NSAttributedString.Key.foregroundColor: UIColor(red: 1, green: 1, blue: 1, alpha: 0.6),NSAttributedString.Key.font :UIFont(name: "Breakers W00", size: 14) ?? UIFont.systemFont(ofSize: 14.0)])
        }
        if let textfield = searchTextField.value(forKey: "searchField") as? UITextField {
            textfield.textColor = UIColor.white
        }
        tableView.backgroundColor = .white
        tableView.separatorStyle = .singleLine
    }
    
    @objc func searchTextChanged(notification:Notification){
            viewModel?.getEvent(searchString: searchTextField.text)
        }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(searchTextChanged), name: UITextField.textDidChangeNotification, object: nil)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func cancelAction(_ sender:UIButton){
        searchTextField.text = ""
        self.searchTextField.endEditing(true)
        self.getEvents(searchString: searchTextField.text)
        
    }
    
    
    
}

extension EventViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: EventTableViewCell.identifier, for: indexPath) as? EventTableViewCell else {return UITableViewCell()}
        if eventsDict?.events?.count ?? 0 > indexPath.row {
            cell.event = eventsDict?.events?[indexPath.row]
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventsDict?.events?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: EventDetailsViewController.sequeIdentifier, sender: eventsDict?.events?[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as? EventDetailsViewController
        destination?.event = sender as? ResultModel
    }
}

extension EventViewController : EventViewModelDelegate {
    
    func getEvent(events: Events) {
        self.eventsDict = events
    }
}

extension EventViewController:UITextFieldDelegate,UISearchBarDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
        {
            self.searchTextField.endEditing(true)
        }
}

