//
//  ResultModel.swift
//  Digital14_iOS_App
//
//  Created by Tripti Kumari on 04/08/21.
//

import Foundation

class ResultModel:Decodable {
    var title:String?
    var url:String?
    var venue:Venue?
    var datetime_local:String?
    var performers:[Performers]?
    
    init(title:String?,url:String?,venue:Venue?,datetime_local:String?) {
        self.title = title
        self.url = url
        self.venue = venue
        self.datetime_local = datetime_local
    }
}

class Performers:Decodable {
    var image:String?
    init(image:String?) {
        self.image = image
     
    }
}

class Venue:Decodable {
    var display_location:String?
    init(display_location:String?) {
        self.display_location = display_location
     
    }
}

struct Events : Decodable {
    private enum CodingKeys : String, CodingKey { case events = "events" }
    let events : [ResultModel]?
    init(events : [ResultModel]?) {
        self.events = events
     
    }
}
