//
//  ApiManager.swift
//  Digital14_iOS_App
//
//  Created by Tripti Kumari on 04/08/21.
//

import Foundation

class APiManager {
    private let baseUrl = Config.baseURL
    
    private let decoder: JSONDecoder
    
    public init(_ decoder: JSONDecoder = JSONDecoder()) {
        self.decoder = decoder
    }
    
    public enum HttpMethod : String {
        case get = "GET"
        case put = "PUT"
        case post = "POST"
        case patch = "PATCH"
        case delete = "DELETE"
    }
    
    private enum RestfulApiSubpath {
        case getSeatGeekResult
        
        var expectsJsonReply: Bool {
            get {
                switch self {
                case .getSeatGeekResult: return true
                    
                }
            }
        }
        
        var getUrlSubpath : String {
            get {
                switch self {
                case .getSeatGeekResult: return "2/events?"
                
                }
            }
        }
    }
    
    func getEvents(searchString:String?,completion:@escaping(Events?) -> Void) {
        let subUrl = "client_id=\(Config.clientId)&q=\(searchString ?? "")"
        
        sendCloudReq(Events.self, subpath: .getSeatGeekResult, subUrl: subUrl, method: .get, bodyArgs: nil) { response in
            completion(response)
        } error: { error in
            print("Error: \(String(describing: error))")
        }
    }
    
    private func sendCloudReq<T: Decodable>(_ objectType: T.Type, subpath: RestfulApiSubpath, subUrl: String,  method: HttpMethod, bodyArgs: [String: Any]?, completion: @escaping(T?) -> Void, error: @escaping(Error?) -> Void) {
        
        
        Utils.shared.showLoader()
    
        guard let urlString = "\(self.baseUrl.absoluteString)\(subpath.getUrlSubpath)\(subUrl)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        var req = URLRequest(url: URL(string: urlString) ?? baseUrl)
        print("API URL: \(req.url ?? baseUrl)")
        req.httpMethod = method.rawValue
        
        if let bodyArgs = bodyArgs {
            guard let jsonData = try? JSONSerialization.data(withJSONObject: bodyArgs) else { assert(false, "invalid json request"); return }
            req.httpBody = jsonData
        }
        req.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
            URLSession.shared.dataTask(with: req) { (data, res, err) in
                
                if err == nil, let status = res as? HTTPURLResponse {
                    print("API Response: \(status.statusCode)")
                        if subpath.expectsJsonReply {
                            if let data = data {
                                let str = String(decoding: data, as: UTF8.self)
                                do {
                                    let response = try self.decoder.decode(T.self, from: data)
                                    Utils.shared.hideLoader()
                                    completion(response)
                                    }
                                catch let error {
                                    print("error: \(error.localizedDescription) *** \(error)")
                                    Utils.shared.hideLoader()
                                    visibleViewController()?.alertNotification(msg: "Invalid response from server!")
                                }
                            }
                        }
                    }
                else if let err = err {
                    error(err)
                    Utils.shared.hideLoader()
                    visibleViewController()?.alertNotification(msg: err.localizedDescription, with: 2)
                }
                else {
                    
                    Utils.shared.hideLoader()
                    visibleViewController()?.alertNotification(msg: "Something went wrong!", with: 2)
                }
            }.resume()
        }
    
    private func showErrorAlert(msg: String?) {
        Utils.shared.hideLoader()
        DispatchQueue.main.async {
            visibleViewController()?.alertNotification(msg: "\(msg ?? "Something went wrong!")", with: 2)
        }
    }
}
