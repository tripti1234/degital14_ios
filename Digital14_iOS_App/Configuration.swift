//
//  Configuration.swift
//  Digital14_iOS_App
//
//  Created by Tripti Kumari on 04/08/21.
//

import Foundation
import UIKit

enum Config {
    
    static var baseURL: URL {
        return try! URL(string: Configuration.value(for: "ServerUrl"))!
    }
    
    static let clientId = "MjI3NzcxMzV8MTYyODA1NTgzMi41MjMyOTUy"
}

enum Configuration {
    
    enum Error: Swift.Error {
        case missingKey, invalidValue
    }

    static func value<T>(for key: String) throws -> T where T: LosslessStringConvertible {
        
        guard let object = Bundle.main.object(forInfoDictionaryKey: key) else {
            throw Error.missingKey
        }

        switch object {
        case let value as T:
            return value
        case let string as String:
            guard let value = T(string) else { fallthrough }
            return value
        default:
            throw Error.invalidValue
        }
    }
    
    
}

extension UIWindow {
    
    var visibleViewController: UIViewController? {
        return UIWindow.visibleVC(vc: self.rootViewController)
    }

    static func visibleVC(vc: UIViewController?) -> UIViewController? {
        if let navigationViewController = vc as? UINavigationController {
            return UIWindow.visibleVC(vc: navigationViewController.visibleViewController)
        }
        else if let tabBarVC = vc as? UITabBarController {
            return UIWindow.visibleVC(vc: tabBarVC.selectedViewController)
        }
        else {
            if let presentedVC = vc?.presentedViewController {
                return UIWindow.visibleVC(vc: presentedVC)
            }
            else {
                return vc
            }
        }
    }
}

public func visibleViewController() -> UIViewController? {
    UIApplication.shared.windows.filter { $0.isKeyWindow }.first?.visibleViewController
}
